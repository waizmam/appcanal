import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ProgramasPage } from '../pages/programas/programas';
import { ProgramaPage } from '../pages/programas/programa';
import { ProgramacaoPage } from '../pages/programacao/programacao';
import { AssistirVideoPage } from '../pages/assistir/assistirVideo';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/popover/login';

import {  TruncatePipe }   from './app.pipe';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//Services
import { VideoService } from './../services/video-service';
import { ProgramacaoService } from './../services/programacao-service';
import { ProgramasService } from './../services/programas-service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Calendar } from '@ionic-native/calendar';

@NgModule({
  declarations: [
    MyApp,
    ProgramasPage,
    ProgramaPage,
    ContactPage,
    HomePage,
    LoginPage,
    AssistirVideoPage,
    ProgramacaoPage,
    TabsPage,
    TruncatePipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProgramasPage,
    ProgramaPage,
    ContactPage,
    LoginPage,
    HomePage,
    AssistirVideoPage,
    ProgramacaoPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SocialSharing,
    Calendar,
    VideoService,
    ProgramacaoService,
    ProgramasService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
