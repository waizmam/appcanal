import { Component } from '@angular/core';
import { ProgramasPage } from '../programas/programas';
import { ProgramacaoPage } from '../programacao/programacao';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tabHome = HomePage;
  tabPrograma = ProgramasPage;
  tabProgramacao = ProgramacaoPage;
  tabSobre = ContactPage;
  tabContato = ContactPage;

  constructor() {

  }
}
