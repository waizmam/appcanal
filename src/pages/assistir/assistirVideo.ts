import { Component } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';

@Component({
  selector: 'page-assistirVideo',
  templateUrl: 'assistirVideo.html'
})
export class AssistirVideoPage {

  public video;
  public programa = [];

  constructor(public navParams: NavParams, public navCtrl: NavController) {
        this.video = this.navParams.get('videoSelecionado');
        this.programa = this.navParams.get('programa');
  }

}
