import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  programacao: any;

  constructor(public navCtrl: NavController, 
    public viewCtrl : ViewController ,public navParams: NavParams) {
      this.programacao = this.navParams.get('programacao');
  }
  public closeModal(){
      this.viewCtrl.dismiss();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
    console.log(this.navParams.get('programacao'));
    
}

}
