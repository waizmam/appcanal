import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { ProgramacaoService } from './../../services/programacao-service';
import * as moment from 'moment';
import { ModalController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Calendar } from '@ionic-native/calendar';

@Component({
  selector: 'page-programacao',
  templateUrl: 'programacao.html'
})
export class ProgramacaoPage {

  programacaoService: ProgramacaoService;
  programacaoDiaria: Array<Object> = [];
  programacaoSemanal: any;
  dia: string;
  diasSemana: Array<any> = [];
  diaSegment: any = ''; 
  pagina: number = 0;
  
  constructor(public navCtrl: NavController,programacaoService: ProgramacaoService, private _loadingCtrl: LoadingController, 
              public modalCtrl: ModalController, public toastCtrl: ToastController, private alertCtrl: AlertController,
              private socialSharing: SocialSharing, private calendar: Calendar) {
      this.programacaoService = programacaoService;         
      moment.locale('pt-BR');
      this.diaSegment = moment().format('YYYY-MM-DD'); 
       
  }

  ngOnInit(){

      let loader = this.carregaLoader(this._loadingCtrl);
      let data = new Date();
      this.dia = moment(data).format('dddd, D [de] YYYY');      

      this.programacaoService
      .programacaoDiaria()
      .subscribe(programacao => {
        this.programacaoDiaria = programacao;         

        loader.dismiss();          
      }, erro => {
        console.log(erro);
        loader.dismiss(); 
      });      

      this.programacaoService
      .programacaoSemanal()
      .subscribe(programacaoSemanal => {
        this.programacaoSemanal = programacaoSemanal.programacao;    

        Object.keys(this.programacaoSemanal).forEach(key=> {
            this.diasSemana.push({'data': key , 'dia': moment(key).format('ddd')});            
        });        

        loader.dismiss();          
      }, erro => {
        console.log(erro);
        loader.dismiss(); 
      }); 


  }


  programacaoPorData(data){

      let loader = this.carregaLoader(this._loadingCtrl);
      this.dia = moment(data).format('dddd, D [de] YYYY');
      this.diaSegment = data; 
      data = moment(data).format('DD/MM/YYYY'); 

      console.log('data: '+ data);
      
      this.programacaoService
      .programacaoDiariaPorData(data,0)
      .subscribe(programacao => {
        this.programacaoDiaria = [];
        this.programacaoDiaria = programacao;   
        
        loader.dismiss();          
      }, erro => {
        console.log(erro);
        loader.dismiss(); 
      });      

  }

  carregaLoader(loaderController : LoadingController){
      let loading = this._loadingCtrl.create({
            content: 'Carregando programação. Aguarde...'
      });
      return loading;
  }

  public openModal(programacao){     
      
      var data = { programacao : programacao };
      var modalPage = this.modalCtrl.create('ModalPage',data);
      modalPage.present();
  }


  doInfinite(infiniteScroll) {

    let totalPagina = this.programacaoDiaria['totalPaginas'];
    let dataProg = moment(this.diaSegment).format('DD/MM/YYYY'); 
    
    if (this.pagina < totalPagina) {
        this.pagina += 1;

        setTimeout(() => {

          this.programacaoService
          .programacaoDiariaPorData(dataProg,this.pagina)
          .subscribe(programacao => {

            Object.keys(programacao.programacaoEncontrados).forEach(key=> {
                this.programacaoDiaria['programacaoEncontrados'].push(programacao.programacaoEncontrados[key]);            
            }); 
   
          }, erro => {
            console.log(erro);
          });  

          infiniteScroll.complete();
        }, 500);

    }else{
      infiniteScroll.enable(false);
      let mensagem = "Fim da Lista...";
      this.presentToast(mensagem);
    }     
    
  }

  presentToast(mensagem : string) {
    let toast = this.toastCtrl.create({
      message: mensagem,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
    });

    toast.present();
  }


  enviarEmail(){
    let alert = this.alertCtrl.create({
      title: 'Sucesso!',
      subTitle: 'Email enviado com sucesso',
      buttons: ['Ok']
    });
    alert.present();
  }

  lembrar(programacao){

    let startDate = new Date(programacao.dia+' '+programacao.hrinicial);
    let endDate = new Date(programacao.dia+' '+programacao.hrfinal);
    
    this.calendar.createEvent(
      programacao.programa, 
      'Aplicativo Canal Saúde', 
      programacao.tema, 
      startDate, 
      endDate).then( result => {
        
          let alert = this.alertCtrl.create({
            title: 'Solicitação de Lembrete!',
            subTitle: 'Avisaremos quando estiver na hora de você assistir seu programa',
            buttons: ['ok']
          });
          alert.present();
      }).catch( erro =>{
          let alert = this.alertCtrl.create({
            title: 'Desculpa!',
            subTitle: 'Não conseguimos agendar...',
            buttons: ['ok']
          });
          alert.present();
      });

    
  }
 
  regularShare(programacao){
    // share(message, subject, file, url)
    this.socialSharing.share(programacao.tema + ' - '+ programacao.hrinicial,null , 
    programacao.url_imagem_programa,'http://www.canal.fiocruz.br/');    
  }

 
}
