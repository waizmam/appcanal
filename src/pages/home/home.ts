import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { VideoService } from './../../services/video-service';
import { AssistirVideoPage } from './../assistir/assistirVideo';
import { PopoverController } from 'ionic-angular';
import { LoginPage } from '../../pages/popover/login';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [VideoService]
})
export class HomePage implements OnInit {

  videoService: VideoService;
  videosDestaques: Array<Object>;  

  constructor(public navCtrl: NavController,videoService: VideoService, private _loadingCtrl: LoadingController,
              public popoverCtrl: PopoverController) {
      this.videoService = videoService;     
  }

  ngOnInit(){

      let loader = this._loadingCtrl.create({
            content: 'Carregando vídeos. Aguarde...'
      });
      loader.present();

      this.videoService
      .listaDestaques()
      .subscribe(videos => {
        this.videosDestaques = videos;        
        loader.dismiss();          
      }, erro => {
        console.log(erro);
        loader.dismiss(); 
      });      


  }

  assistirVideo(video){
        this.navCtrl.push(AssistirVideoPage,{videoSelecionado: video});
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(LoginPage);
    popover.present({
      ev: myEvent
    });
  }    
  
  
}
