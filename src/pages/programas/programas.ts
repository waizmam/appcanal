import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { ProgramasService } from './../../services/programas-service';
import { ProgramaPage } from './programa';

@Component({
  selector: 'page-programas',
  templateUrl: 'programas.html'
})
export class ProgramasPage {

  programasService : ProgramasService;
  programas: Array<Object>;

  constructor(public navCtrl: NavController,programasService: ProgramasService, private _loadingCtrl: LoadingController) {
      this.programasService = programasService; 
  }

  ngOnInit(){
      let loader = this.carregaLoader(this._loadingCtrl);

      this.programasService
      .listaProgramas()
      .subscribe(programas => {
        this.programas = programas;    
        //console.log(this.programas); 
        loader.dismiss();          
      }, erro => {
        console.log(erro);
        loader.dismiss(); 
      });      


   }

   carregaLoader(loaderController : LoadingController){
      let loading = this._loadingCtrl.create({
            content: 'Carregando programas. Aguarde...'
      });
      return loading;
  }

  programaSelecionado(programa){
      this.navCtrl.push(ProgramaPage,{'programa':programa});
  }

}
