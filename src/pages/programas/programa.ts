import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams, ToastController } from 'ionic-angular';
import { VideoService } from './../../services/video-service';
import { AssistirVideoPage } from './../assistir/assistirVideo';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-programa',
  templateUrl: 'programa.html'
})
export class ProgramaPage {

  videoService : VideoService;
  videoPrograma: Array<Object> = [];
  programa: Object;
  pagina: number = 0;

  constructor(public navCtrl: NavController,videoService: VideoService, private _loadingCtrl: LoadingController,
              public navParams: NavParams,public toastCtrl: ToastController, private socialSharing: SocialSharing) {
      this.videoService = videoService;     
      this.programa = this.navParams.get('programa');  
  }

  ngOnInit(){

      let loader = this.carregaLoader(this._loadingCtrl);      

      this.videoService
      .videosPorPrograma(this.programa,0)
      .subscribe(videos => {
        this.videoPrograma = videos;    
        //console.log(this.videoPrograma); 
        loader.dismiss();          
      }, erro => {
        console.log(erro);
        loader.dismiss(); 
      });      


   }

   carregaLoader(loaderController : LoadingController){
      let loading = this._loadingCtrl.create({
            content: 'Carregando programas. Aguarde...'
      });
      return loading;
  }

  assistirVideo(video){
        this.navCtrl.push(AssistirVideoPage,{
          videoSelecionado: video,
          programa : this.programa
        });
   }


   doInfinite(infiniteScroll) {

    let totalPagina = this.videoPrograma['totalPaginas'];    
    
    if (this.pagina < totalPagina) {
        this.pagina += 1;

        setTimeout(() => {

          this.videoService
          .videosPorPrograma(this.programa,this.pagina)
          .subscribe(videos => {
            this.videoPrograma = videos;    
           
            Object.keys(videos.videosEncontrados).forEach(key=> {
                this.videoPrograma['videosEncontrados'].push(videos.videosEncontrados[key]);            
            }); 
   
          }, erro => {
            console.log(erro);
          });  

          infiniteScroll.complete();
        }, 700);

    }else{
      infiniteScroll.enable(false);
      let mensagem = "Fim da Lista...";
      this.presentToast(mensagem);
    }     
    
  }

  presentToast(mensagem : string) {
    let toast = this.toastCtrl.create({
      message: mensagem,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
    });

    toast.present();
  }

  regularShare(video){
    // share(message, subject, file, url)
    let urlvideofinal = video.imagem.split('.');
    this.socialSharing.share(video.nome, video.descricao, 
    video.url_imagem_video, 'http://www.canal.fiocruz.br/video/index.php?v='+urlvideofinal[0]);
    
  }


}
