import { Http, Headers,RequestOptions, URLSearchParams} from '@angular/http';   
import { Injectable } from '@angular/core';

@Injectable()
export class ProgramacaoService{

    http: Http;    
    url: string = 'http://wsapic3n3l:c4n4lwebs3rv3c3@www.api.canalsaude.fiocruz.br/ws/api/Programacao/';
    headers: Headers;
    userAPI: string = 'wsapic3n3l';
    senhaAPI: string = 'c4n4lwebs3rv3c3';

   

    constructor(http: Http){
        this.http = http;
        this.headers = new Headers();

        this.headers.append("Authorization", "Basic " + btoa(this.userAPI + ":" + this.senhaAPI)); 
        this.headers.append("Access-Control-Allow-Origin", "*");
        this.headers.append('Access-Control-Allow-Headers', 'X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method');
        this.headers.append("Content-Type", "application/x-www-form-urlencoded");
       
    }

   
    programacaoDiaria(){          
        return this.http.get(this.url+'programacaoDiaria',{ headers: this.headers })
        .map(res => res.json()); 
    }

    programacaoDiariaPorData(data,pagina){
        let myParams = new URLSearchParams();
        myParams.append('data', data);
        myParams.append('page', pagina);
         let options = new RequestOptions({ headers: this.headers, params: myParams });    
        return this.http.get(this.url+'programacaoDiaria',options)
        .map(res => res.json()); 
    }

    programacaoSemanal(){
        return this.http.get(this.url+'programacaoSemanal',{ headers: this.headers })
        .map(res => res.json()); 
    }

  

}